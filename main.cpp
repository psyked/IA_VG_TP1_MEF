#include <fstream>
#include <time.h>

#include "Locations.h"
#include "Miner.h"
#include "MinersWife.h"

#include "Cowboy.h"

#include "EntityManager.h"
#include "MessageDispatcher.h"
#include "misc/ConsoleUtils.h"
#include "EntityNames.h"


std::ofstream os;

int main()
{
//define this to send output to a text file (see locations.h)
#ifdef TEXTOUTPUT
  os.open("output.txt");
#endif

  //seed random number generator
  srand((unsigned) time(NULL));

  //create a miner
  Miner* Bob = new Miner(ent_Miner_Bob);
  Bob->setColorText(FOREGROUND_RED | FOREGROUND_INTENSITY);
  //create his wife
  MinersWife* Elsa = new MinersWife(ent_Elsa);
  Elsa->setColorText(FOREGROUND_GREEN | FOREGROUND_INTENSITY);

  //create cowboy
  Cowboy* Toto = new Cowboy(ent_cowboy1, GoInSaloonAndDrink::Instance(), saloon);
  Toto->setColorText(FOREGROUND_BLUE | FOREGROUND_INTENSITY);
  Cowboy* Titi = new Cowboy(ent_cowboy2);
  Titi->setColorText(FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);

  //register them with the entity manager
  EntityMgr->RegisterEntity(Bob);
  EntityMgr->RegisterEntity(Elsa);

  EntityMgr->RegisterEntity(Toto);
  EntityMgr->RegisterEntity(Titi);

  //run Bob and Elsa through a few Update calls
  for (int i=0; i<30; ++i)
  { 
	Bob->Update();
    Elsa->Update();

	Toto->Update();
	Titi->Update();

    //dispatch any delayed messages
    Dispatch->DispatchDelayedMessages();

    Sleep(100);
  }

  //tidy up
  delete Bob;
  delete Elsa;

  delete Toto;
  delete Titi;
  //wait for a keypress before exiting
  PressAnyKeyToContinue();


  return 0;
}






