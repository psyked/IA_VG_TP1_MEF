= Intelligence artificielle pour les Jeux Vidéo TP1 : Westworld

:toc:

== Introduction
L'équipe constitué de :

* Edouard FRANCOIS - FRAE22049505
* David Klug - KLUD13059501
* et Adrien Guillerme - GUIA01079405

a implémenté de nouvelles fonctionalités et agents pour le jeu Westworld de Mat Buckland.

== Documents annexes
Vous pouvez trouvez joint dans le dossier **annexes** les diagrammes des machines d'états
modifiés du mineur, de sa femme et de l'agent saoûlard. Ces diagrammes peuvent 
être modifiés depuis link:https://www.draw.io[ce site].

Dans ce dossier vous trouverez aussi trois imprimés écran du programme en fonctionnement
ainsi que des commentaires sur le comportement des agents.

== Ajouts
=== Nouvel agent(s)
Pour ajouter un agent supplémentaire interagissant avec le mineur Bob, nous avons
pensé à un cowboy (cf. diagramme d'états joint).

Ainsi cet agent à 3 états différent et un état global :

* `GoOutOfTown`
** Il passe un certain
temps en dehors de la ville (sans avoir plus de précisions :chasse, s'occupe du 
bétail, camp, etc.). Puis après un certain temps passé à l'extérieur de la ville,
il rentre pour boire au saloon.
* `GoInSaloonAndDrink`
** Lorsqu'il y rentre, il salue tous les agents s'y trouvant.
** A chaque exécution, il boit un verre ce qui augmente son niveau d'ivresse.
** Si ce niveau dépasse celui de saoulerie, il passe dans l'état `IWantToFight` ci-dessous.
** Si ce niveau dépasse celui de fatigue, il retourne dans l'état `GoOutOfTown`.
*** S'il reçoit un message `Msg_IWantToFight` dans cet état, il y répond avec un `Act_Punch`.
*** S'il reçoit un message `Act_Punch` dans cet état, alors il s'en moque (ce cas 
ne devrait pas se produire, ce message n'est envoyé que par un cowboy après provocation.
Or dans cet état, le cowboy ne peut pas provoquer quelqu'un).
* `IWantToFight`
** L'agent continue de boire et envoit un message `Msg_IWantToFight` au 1er agent.
détecté dans au même endroit que lui (s'il y en a un).
** Si son niveau d'ivresse dépasse celui de fatigue, il retourne dans l'état `GoOutOfTown`.
*** S'il reçoit un message `Msg_IWantToFight` dans cet état, il y répond avec un `Act_Punch`.
*** S'il reçoit un message `Act_Punch` dans cet état, alors il retourne dans l'état `GoOutOfTown`.
* `Global`
** S'il reçoit un `Msg_Hi`, il salut l'agent le lui ayant envoyé.

=== Recherche de personnage
Un personnage peut envoyer un message à un autre quelles que soient leurs locations
respectives. Pour éviter des problèmes tels que la femme du mineur lui disant 
que le ragout est prêt mais Bob est déjà ressortit et le message n'atteind jamais
le destinataire. Pour palier ce problème, on pourrait ajouter une gestion de ce
message dans un état global du mineur mais cela voudrait dire que des agents peuvent
communiquer alors que leurs locations sont incompatibles. 

Ainsi, on implémenta dans le MessageDispacher :

* la méthode `location WhereIsAgent(int ID)`
prennant l'id d'un agent et renvoyant la `location` de ce dernier.
* Ainsi que la méthode `bool WhoIsWithMe(location mylocation, int* ptable, int* psize)`
chargeant les IDs des agents dans la location donnée dans le tableau `ptable` et
renvoyant `true` si des agents ont été trouvé, `false` sinon.

Pour arriver à ce résultat, nous modifions la classe `BaseGameEntity` pour exposer
les accesseurs virtuels de la location des classes héritants de cette classe. N'ayant
que des agents avec location héritant de `BaseGameEntity`, l'ajout des ces méthodes
ne modifient pas le fonctionnement des classes.

=== Divers

* Ajout d'une fonction donnant le nom de l'enum location en cours pour pouvoir écrire une
location dans la console.

== Modifications
Quelque modification furent ajoutés aux fonctions existantes :

* Ajout d'aléatoire dans la fréquence de certaines actions :
** La quantité de verres que boit le cowboy
** Le temps passé en dehors de la ville pour le cowboy
** Le nombre de nuggets d'or que le mineur trouve en minant.
* Ajout d'options des contruscteurs des personnages pour ajouter une état et une 
location de départ.
* Ajout d'un état global au mineur et a sa femme lui permettant de répondre 
lorsqu'un agent
lui envoit un message 'Hi'.
* La femme du mineur ne lui dit plus que le ragoût est prêt s'il n'est plus à la maison.
** La femme va alors rechercher son mari pour lui annoncer : elle va a la même location que lui, 
lui dit que le ragoût est prêt et retourne faire du ménage.
** Si le mari rentre et qu'elle le cherchait pour lui dire que le ragoût est prêt, elle n'en 
reprépare pas un autre.
* On ajouta l'attribut `colorUsed` ainsi que ses assesseur à `BaseGameEntity` pour
pouvoir changer et choisir facilement la couleur du text à aficher pour chacun
des agents. Tout en modifiant les appels à `SetTextColor()` pour qu'ils utilisent
cet attribut.
* De plus, les agents ont maintenant dans leurs constructeurs des variables définissant
la location et l'état d'où ils doivent commencer.

== Compilation
Le programme suivant a été compilé avec succès en mode Debug et Release depuis
Visual Studio 2017. Pour la compilation, il est neccessaire d'avoir le dossier 
`Common` des TPs de Mat Buckland dans un dossier parent de celui parent où est contenu
le fichier .sln. Si ce n'est pas le cas, il suffit de changer le path depuis
Projet>Propriétés>C/C++/ligne de commande puis ajouter votre path dans l'encart 
`options supplémentaires`. De base, nous incluons les fichiers avec 
`-I ../../Common`