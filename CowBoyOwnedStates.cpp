#include "CowboyOwnedStates.h"
#include "fsm/State.h"
#include "Cowboy.h"
#include "Locations.h"
#include "messaging/Telegram.h"
#include "MessageDispatcher.h"
#include "MessageTypes.h"
#include "Time/CrudeTimer.h"
#include "EntityNames.h"

#include <iostream>
using std::cout;


#ifdef TEXTOUTPUT
#include <fstream>
extern std::ofstream os;
#define cout os
#endif

//-----------------------------------------------------------------------Global state

CowboyGlobalState* CowboyGlobalState::Instance()
{
	static CowboyGlobalState instance;

	return &instance;
}


bool CowboyGlobalState::OnMessage(Cowboy* pagent, const Telegram& msg)
{
	SetTextColor(BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	switch (msg.Msg)
	{
	case Msg_Hi:

		cout << "\nMessage handled by " << GetNameOfEntity(pagent->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(pagent->colorText());

		cout << "\n" << GetNameOfEntity(pagent->ID())
			<< ": Hi, " << GetNameOfEntity(msg.Sender) << " !";

	

		return true;

	}//end switch

	return false;
}



//------------------------------------------------------------------------methods for GoOutOfTown
GoOutOfTown* GoOutOfTown::Instance()
{
	static GoOutOfTown instance;

	return &instance;
}


void GoOutOfTown::Enter(Cowboy* pCowboy)
{
	//if the cowboy is not already located at the goldmine, he must
	//change location to outside
	if (pCowboy->Location() != outside)
	{
		cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": " << "Walkin'outside of town";

		pCowboy->ChangeLocation(outside);
		pCowboy->SetTimeOutside(0);
	}
}


void GoOutOfTown::Execute(Cowboy* pCowboy)
{
	// Now the cowboy is outside, he will work, sleep, etc. until he wants again
	// to get drunk
	pCowboy->Rest();
	SetTextColor(pCowboy->colorText());

	cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": " << "Passin' time outside";

	if (pCowboy->WantToGetDrunk())
	{
		pCowboy->GetFSM()->ChangeState(GoInSaloonAndDrink::Instance());
	}
}


void GoOutOfTown::Exit(Cowboy* pCowboy)
{
	SetTextColor(pCowboy->colorText());

	cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": "
		<< "Ah'm savin' this hard work for later";
}


bool GoOutOfTown::OnMessage(Cowboy* pCowboy, const Telegram& msg)
{
	//send msg to global message handler
	return false;
}

//------------------------------------------------------------------------methods for GoInSaloonAndDrink

GoInSaloonAndDrink* GoInSaloonAndDrink::Instance()
{
	static GoInSaloonAndDrink instance;

	return &instance;
}

void GoInSaloonAndDrink::Enter(Cowboy* pCowboy)
{
	SetTextColor(pCowboy->colorText());

	if (pCowboy->Location() != saloon)
	{
		cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": " << "Goin' to the saloon ";

		pCowboy->ChangeLocation(saloon);
	}
	int tableNeighbors[1];
	int *ptable = tableNeighbors;
	int psizeTable = 0;

	if (!Dispatch->WhoIsWithMe(pCowboy->Location(), &ptable, &psizeTable))
	{
		delete[] ptable;
		return;
	}
	if (psizeTable <= 1)
	{
		delete[] ptable;
		cout << "oh, no one here";
		return;
	}
	for (int i = 0; i < psizeTable; i++)
	{
		if (ptable[i] != pCowboy->ID())
			Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY, //time delay
				pCowboy->ID(),        //ID of sender
				ptable[i],            //ID of recipient
				Msg_Hi,   //the message
				NO_ADDITIONAL_INFO);
	}
	delete[] ptable;
}


void GoInSaloonAndDrink::Execute(Cowboy* pCowboy)
{
	pCowboy->Drink();
	SetTextColor(pCowboy->colorText());


	cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": "
		<< "Drinking, total of drinks: " << pCowboy->GetDrinks();

	//wealthy enough to have a well earned rest?
	if (pCowboy->WantToFight())
	{
		pCowboy->GetFSM()->ChangeState(WantToFight::Instance());
	}
	if (pCowboy->WantToLeave())
	{
		cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": "
			<< "My head spins way too fast, i'm gonna reach my camp";

		pCowboy->GetFSM()->ChangeState(GoOutOfTown::Instance());
	}
}


void GoInSaloonAndDrink::Exit(Cowboy* pCowboy)
{
	SetTextColor(pCowboy->colorText());

	if (pCowboy->WantToFight())
	{
		cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": " << "Gettin' drunk";
	}
	else
	{
		cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": " << "Leavin' the saloon";
	}
	
}


bool GoInSaloonAndDrink::OnMessage(Cowboy* pCowboy, const Telegram& msg)
{
	SetTextColor(BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	switch (msg.Msg)
	{

	case Msg_IWantToFight:

		cout << "\nMessage handled by " << GetNameOfEntity(pCowboy->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(pCowboy->colorText());

		cout << "\n" << GetNameOfEntity(pCowboy->ID())
			<< ": Let's dance boi ! [punch " << GetNameOfEntity(msg.Sender) << "]";

		Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY, //time delay
			pCowboy->ID(),        //ID of sender
			msg.Sender,            //ID of recipient
			Act_Punch,   //the message
			NO_ADDITIONAL_INFO);
		return true;

	case Act_Punch:

		cout << "\nMessage handled by " << GetNameOfEntity(pCowboy->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(pCowboy->colorText());

		cout << "\n" << GetNameOfEntity(pCowboy->ID())
			<< "Was it supposed to hurt ?";

		return true;

	}//end switch

	return false; //send message to global message handler
}
//------------------------------------------------------------------------WantToFight methodes

WantToFight* WantToFight::Instance()
{
	static WantToFight instance;

	return &instance;
}

void WantToFight::Enter(Cowboy* pCowboy)
{
	if (pCowboy->Location() != saloon)
	{
		SetTextColor(pCowboy->colorText());
		cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": " << "Goin' to the saloon to fight";

		pCowboy->ChangeLocation(saloon);
	}
}


void WantToFight::Execute(Cowboy* pCowboy)
{
	pCowboy->Drink();
	SetTextColor(pCowboy->colorText());
	cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": "
		<< "Drinking, total of drinks: " << pCowboy->GetDrinks();

	cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": "
		<< "Who said i've problems with alcohol !?";

	int tableNeighbors[1];
	int *ptable = tableNeighbors;
	int psizeTable = 0;

	if (!Dispatch->WhoIsWithMe(pCowboy->Location(), &ptable, &psizeTable))
	{
		delete[] ptable;
		return;
	}
	if (psizeTable <= 1)
	{
		cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": "
			<< "Hum, no one, should be in my mind ...";
	}
	for (int i = 0; i < psizeTable; i++)
	{
		if (ptable[i] != pCowboy->ID())
		{ // threats only the 1rst agent he sees
			Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY, //time delay
				pCowboy->ID(),        //ID of sender
				ptable[i],            //ID of recipient
				Msg_IWantToFight,   //the message
				NO_ADDITIONAL_INFO);

			delete[] ptable;
			return;
		}
	}
	
	if (pCowboy->WantToLeave())
	{
		SetTextColor(pCowboy->colorText());
		cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": "
			<< "My head spins way too fast, i'm gonna reach my camp";

		pCowboy->GetFSM()->ChangeState(GoOutOfTown::Instance());
	}
}


void WantToFight::Exit(Cowboy* pCowboy)
{
	SetTextColor(pCowboy->colorText());
	cout << "\n" << GetNameOfEntity(pCowboy->ID()) << ": " << "Leavin' the saloon";
}


bool WantToFight::OnMessage(Cowboy* pCowboy, const Telegram& msg)
{
	SetTextColor(BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	switch (msg.Msg)
	{

	case Msg_IWantToFight:

		cout << "\nMessage handled by " << GetNameOfEntity(pCowboy->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(pCowboy->colorText());

		cout << "\n" << GetNameOfEntity(pCowboy->ID())
			<< ": Let's dance boi ! [punch " << GetNameOfEntity(msg.Sender) << "]";

		Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY, //time delay
			pCowboy->ID(),        //ID of sender
			msg.Sender,            //ID of recipient
			Act_Punch,   //the message
			NO_ADDITIONAL_INFO);
		return true;

	case Act_Punch:

		cout << "\nMessage handled by " << GetNameOfEntity(pCowboy->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(pCowboy->colorText());

		cout << "\n" << GetNameOfEntity(pCowboy->ID())
			<< " : Ouch, okay, okay, you win this time ...";

		pCowboy->GetFSM()->ChangeState(GoOutOfTown::Instance());

		return true;

	}//end switch

	return false; //send message to global message handler
}