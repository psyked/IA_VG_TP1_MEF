#ifndef COWBOY_H
#define COWBOY_H
//------------------------------------------------------------------------
//
//  Name:   Cowboy.h
//
//  Desc:   A class defining a cowboy.
//
//
//------------------------------------------------------------------------
#include <string>
#include <cassert>
#include <iostream>

#include "BaseGameEntity.h"
#include "Locations.h"
#include "misc/ConsoleUtils.h"
#include "CowboyOwnedStates.h"
#include "fsm/StateMachine.h"

template <class entity_type> class State; //pre-fixed with "template <class entity_type> " for vs8 compatibility

struct Telegram;

// the amount of time spent before returning to town
const int TimeOutsideLevel = 7;
// the number of drinks before being drunk and bullying
const int DrunkLevel = 3;
// the number of drinks bofore leaving town
const int DrinkMaxLevel = 5;



class Cowboy : public BaseGameEntity
{
private:

	//an instance of the state machine class
	StateMachine<Cowboy>*  m_pStateMachine;

	location_type         m_Location;

	int                   m_iTimeOutside;

	int                   m_iDrinks;

public:

	Cowboy(int id, State<Cowboy>* state= GoOutOfTown::Instance(), location_type location = outside) :m_Location(location),
		m_iTimeOutside(0),
		m_iDrinks(0),
		BaseGameEntity(id)

	{
		//set up state machine
		m_pStateMachine = new StateMachine<Cowboy>(this);

		m_pStateMachine->SetCurrentState(state);

		m_pStateMachine->SetGlobalState(CowboyGlobalState::Instance());

	}

	~Cowboy() { delete m_pStateMachine; }

	//this must be implemented
	void Update();

	//so must this
	virtual bool  HandleMessage(const Telegram& msg);


	StateMachine<Cowboy>* GetFSM()const { return m_pStateMachine; }



	//-------------------------------------------------------------accessors
	location_type Location()const { return m_Location; }
	void          ChangeLocation(location_type loc) { m_Location = loc; }

	bool WantToGetDrunk()const;
	bool WantToFight()const;
	bool WantToLeave()const;
	void Rest();
	void Drink();

	
	int           GetDrinks()const { return m_iDrinks; }
	
	int           GetTimeOutside()const { return m_iTimeOutside; }
	void          SetTimeOutside(int val) { m_iTimeOutside = val; }
	
	};



#endif
