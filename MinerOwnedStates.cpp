#include "MinerOwnedStates.h"
#include "fsm/State.h"
#include "Miner.h"
#include "Locations.h"
#include "messaging/Telegram.h"
#include "MessageDispatcher.h"
#include "MessageTypes.h"
#include "Time/CrudeTimer.h"
#include "EntityNames.h"

#include <iostream>
using std::cout;


#ifdef TEXTOUTPUT
#include <fstream>
extern std::ofstream os;
#define cout os
#endif


//-----------------------------------------------------------------------Global state

MinerGlobalState* MinerGlobalState::Instance()
{
	static MinerGlobalState instance;

	return &instance;
}


bool MinerGlobalState::OnMessage(Miner* pagent, const Telegram& msg)
{
	SetTextColor(BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	switch (msg.Msg)
	{
	case Msg_Hi:

		cout << "\nMessage handled by " << GetNameOfEntity(pagent->ID())
			<< " at time: " << Clock->GetCurrentTime();
		SetTextColor(pagent->colorText());


		cout << "\n" << GetNameOfEntity(pagent->ID())
			<< ": Hi, " << GetNameOfEntity(msg.Sender) << " !";

		return true;

	case Msg_StewReady:

		cout << "\nMessage handled by " << GetNameOfEntity(pagent->ID())
			<< " at time: " << Clock->GetCurrentTime();
		SetTextColor(pagent->colorText());


		cout << "\n" << GetNameOfEntity(pagent->ID())
			<< ": Okay Hun, ahm a comin'!";

		pagent->GetFSM()->ChangeState(EatStew::Instance());

		return true;

	}//end switch

	return false;
}



//------------------------------------------------------------------------methods for EnterMineAndDigForNugget
EnterMineAndDigForNugget* EnterMineAndDigForNugget::Instance()
{
	static EnterMineAndDigForNugget instance;

	return &instance;
}


void EnterMineAndDigForNugget::Enter(Miner* pMiner)
{
	//if the miner is not already located at the goldmine, he must
	//change location to the gold mine
	if (pMiner->Location() != goldmine)
	{
		SetTextColor(pMiner->colorText());
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Walkin' to the goldmine";

		pMiner->ChangeLocation(goldmine);
	}
}


void EnterMineAndDigForNugget::Execute(Miner* pMiner)
{
	//Now the miner is at the goldmine he digs for gold until he
	//is carrying in excess of MaxNuggets. If he gets thirsty during
	//his digging he packs up work for a while and changes state to
	//gp to the saloon for a whiskey.
	int nbPicked = rand() % 3 + 0;
	pMiner->AddToGoldCarried(nbPicked);

	pMiner->IncreaseFatigue();
	char plural = 's';
	if (nbPicked < 2) plural = ' ';
	SetTextColor(pMiner->colorText());
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Pickin' up " << nbPicked << " nugget" << plural;

	//if enough gold mined, go and put it in the bank
	if (pMiner->PocketsFull())
	{
		pMiner->GetFSM()->ChangeState(VisitBankAndDepositGold::Instance());
	}

	if (pMiner->Thirsty())
	{
		pMiner->GetFSM()->ChangeState(QuenchThirst::Instance());
	}
}


void EnterMineAndDigForNugget::Exit(Miner* pMiner)
{
	SetTextColor(pMiner->colorText());
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": "
		<< "Ah'm leavin' the goldmine with mah pockets full o' sweet gold";
}


bool EnterMineAndDigForNugget::OnMessage(Miner* pMiner, const Telegram& msg)
{
	//send msg to global message handler
	return false;
}

//------------------------------------------------------------------------methods for VisitBankAndDepositGold

VisitBankAndDepositGold* VisitBankAndDepositGold::Instance()
{
	static VisitBankAndDepositGold instance;

	return &instance;
}

void VisitBankAndDepositGold::Enter(Miner* pMiner)
{
	//on entry the miner makes sure he is located at the bank
	if (pMiner->Location() != bank)
	{
		SetTextColor(pMiner->colorText());
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Goin' to the bank. Yes siree";

		pMiner->ChangeLocation(bank);
	}
}


void VisitBankAndDepositGold::Execute(Miner* pMiner)
{
	//deposit the gold
	pMiner->AddToWealth(pMiner->GoldCarried());

	pMiner->SetGoldCarried(0);

	SetTextColor(pMiner->colorText());
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": "
		<< "Depositing gold. Total savings now: " << pMiner->Wealth();

	//wealthy enough to have a well earned rest?
	if (pMiner->Wealth() >= ComfortLevel)
	{
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": "
			<< "WooHoo! Rich enough for now. Back home to mah li'lle lady";

		pMiner->GetFSM()->ChangeState(GoHomeAndSleepTilRested::Instance());
	}

	//otherwise get more gold
	else
	{
		pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
	}
}


void VisitBankAndDepositGold::Exit(Miner* pMiner)
{
	SetTextColor(pMiner->colorText());
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Leavin' the bank";
}


bool VisitBankAndDepositGold::OnMessage(Miner* pMiner, const Telegram& msg)
{
	//send msg to global message handler
	return false;
}
//------------------------------------------------------------------------methods for GoHomeAndSleepTilRested

GoHomeAndSleepTilRested* GoHomeAndSleepTilRested::Instance()
{
	static GoHomeAndSleepTilRested instance;

	return &instance;
}

void GoHomeAndSleepTilRested::Enter(Miner* pMiner)
{
	if (pMiner->Location() != shack)
	{
		SetTextColor(pMiner->colorText());
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Walkin' home";

		pMiner->ChangeLocation(shack);

		//let the wife know I'm home
		Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY, //time delay
			pMiner->ID(),        //ID of sender
			ent_Elsa,            //ID of recipient
			Msg_HiHoneyImHome,   //the message
			NO_ADDITIONAL_INFO);
	}
	pMiner->ChangeLocation(shack);

}

void GoHomeAndSleepTilRested::Execute(Miner* pMiner)
{
	SetTextColor(pMiner->colorText());
	//if miner is not fatigued start to dig for nuggets again.
	if (!pMiner->Fatigued())
	{
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": "
			<< "All mah fatigue has drained away. Time to find more gold!";

		pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
	}

	else
	{
		//sleep
		pMiner->DecreaseFatigue();

		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "ZZZZ... ";
	}
}

void GoHomeAndSleepTilRested::Exit(Miner* pMiner)
{
}


bool GoHomeAndSleepTilRested::OnMessage(Miner* pMiner, const Telegram& msg)
{
	SetTextColor(BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	switch (msg.Msg)
	{
	case Msg_StewReady:

		cout << "\nMessage handled by " << GetNameOfEntity(pMiner->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(pMiner->colorText());

		cout << "\n" << GetNameOfEntity(pMiner->ID())
			<< ": Okay Hun, ahm a comin'!";

		pMiner->GetFSM()->ChangeState(EatStew::Instance());

		return true;

	}//end switch

	return false; //send message to global message handler
}

//------------------------------------------------------------------------QuenchThirst

QuenchThirst* QuenchThirst::Instance()
{
	static QuenchThirst instance;

	return &instance;
}

void QuenchThirst::Enter(Miner* pMiner)
{
	if (pMiner->Location() != saloon)
	{
		pMiner->ChangeLocation(saloon);

		SetTextColor(pMiner->colorText());
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Boy, ah sure is thusty! Walking to the saloon";

		int tableNeighbors[1];
		int *ptable = tableNeighbors;
		int psizeTable = 0;
		
		if (!Dispatch->WhoIsWithMe(pMiner->Location(), &ptable, &psizeTable))
		{
			delete[] ptable;
			return;
		}
		if (psizeTable <= 1)
		{
			delete[] ptable;
			cout << "oh, no one here";
			return;
		}
		for(int i = 0; i < psizeTable; i++)
			if(ptable[i] != pMiner->ID())
				Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY, //time delay
					pMiner->ID(),        //ID of sender
					ptable[i],            //ID of recipient
					Msg_Hi,   //the message
					NO_ADDITIONAL_INFO);

		delete[] ptable;
	}
}

void QuenchThirst::Execute(Miner* pMiner)
{
	pMiner->BuyAndDrinkAWhiskey();

	SetTextColor(pMiner->colorText());
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "That's mighty fine sippin' liquer";

	pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
}


void QuenchThirst::Exit(Miner* pMiner)
{
	SetTextColor(pMiner->colorText());
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Leaving the saloon, ";
	if(!pMiner->Thirsty())
		cout<<"feelin' good";
	else
		cout << "damn, still thirsty";

}


bool QuenchThirst::OnMessage(Miner* pMiner, const Telegram& msg)
{
	SetTextColor(BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	switch (msg.Msg)
	{
	case Msg_Hi:

		cout << "\nMessage handled by " << GetNameOfEntity(pMiner->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(pMiner->colorText());

		cout << "\n" << GetNameOfEntity(pMiner->ID())
			<< ": Hi, guy'!";

		return true;

	case Msg_IWantToFight:

		cout << "\nMessage handled by " << GetNameOfEntity(pMiner->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(pMiner->colorText());

		cout << "\n" << GetNameOfEntity(pMiner->ID())
			<< ": Hey guy, i don't want any worry !";

		pMiner->GetFSM()->ChangeState(GoHomeAndSleepTilRested::Instance());


	}//end switch

	return false; //send message to global message handler
}

//------------------------------------------------------------------------EatStew

EatStew* EatStew::Instance()
{
	static EatStew instance;

	return &instance;
}


void EatStew::Enter(Miner* pMiner)
{
	SetTextColor(pMiner->colorText());
	if (pMiner->Location() != shack)
	{
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Goin home to Eat";
		pMiner->ChangeLocation(shack);
	}
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Smells Reaaal goood Elsa!";
	
}

void EatStew::Execute(Miner* pMiner)
{
	SetTextColor(pMiner->colorText());
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Tastes real good too!";

	pMiner->GetFSM()->RevertToPreviousState();
}

void EatStew::Exit(Miner* pMiner)
{
	SetTextColor(pMiner->colorText());
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Thankya li'lle lady. Ah better get back to whatever ah wuz doin'";
}


bool EatStew::OnMessage(Miner* pMiner, const Telegram& msg)
{
	//send msg to global message handler
	return false;
}


