#ifndef LOCATIONS_H
#define LOCATIONS_H


enum location_type
{
  shack,
  goldmine,
  bank,
  saloon,
  outside
};



inline std::string locationToString(int loc)
{
	switch (loc)
	{
	case shack:
		return "shack";
	case goldmine:
		return "goldmine";
	case bank:
		return "bank";
	case saloon:
		return "saloon";
	case outside:
		return "outside";
	default:
		return "Not recognized!";
	}
}

//uncomment this to send the output to a text file
//#define TEXTOUTPUT




#endif