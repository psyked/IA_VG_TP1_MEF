#ifndef MESSAGE_TYPES
#define MESSAGE_TYPES

#include <string>

enum message_type
{
  Msg_HiHoneyImHome,
  Msg_StewReady,
  Msg_IWantToFight,
  Msg_Hi,
  Act_Punch
};


inline std::string MsgToStr(int msg)
{
  switch (msg)
  {
  case Msg_HiHoneyImHome:
    
    return "HiHoneyImHome"; 

  case Msg_StewReady:
    
    return "StewReady";

  case Msg_IWantToFight:
	  return "Who said that !?";

  case Msg_Hi:
	  return "Hi folks";

  case Act_Punch:
	  return "Take that !";

  default:

    return "Not recognized!";
  }
}

#endif