#ifndef NAMES_H
#define NAMES_H

#include <string>

enum
{  // please update GetSizeEnum if you add an element
	ent_Miner_Bob,

	ent_Elsa,

	// new ones
	ent_cowboy1,

	ent_cowboy2

};

inline int GetSizeEnum()
{  // is used to iterate through the enum ... for the future it should be 
   // replaced by an array to allow adding agents dynamically
	return 4;
}

inline std::string GetNameOfEntity(int n)
{
  switch(n)
  {
  case ent_Miner_Bob:

    return "Miner Bob";

  case 	ent_cowboy1 :

	  return "lonesome cowboy";

  case 	ent_cowboy2:

	  return "Cowboy John";

  case ent_Elsa:
    
    return "Elsa"; 

  default:

    return "UNKNOWN!";
  }
}

#endif