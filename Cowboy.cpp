#include "Cowboy.h"

bool Cowboy::HandleMessage(const Telegram& msg)
{
	return m_pStateMachine->HandleMessage(msg);
}


void Cowboy::Update()
{
	SetTextColor(FOREGROUND_BLUE | FOREGROUND_INTENSITY);

	m_pStateMachine->Update();
}

void Cowboy::Rest()
{
	m_iTimeOutside += rand() % 2 + 1;
	if(m_iDrinks>0)
		m_iDrinks--;
}

void Cowboy::Drink()
{
	m_iDrinks += rand() % 2 + 1;
}


bool Cowboy::WantToGetDrunk()const
{
	return m_iTimeOutside >= TimeOutsideLevel;
}

bool Cowboy::WantToFight()const
{
	return m_iDrinks >= DrunkLevel;

}

bool Cowboy::WantToLeave()const
{
	return m_iDrinks >= DrinkMaxLevel;
}