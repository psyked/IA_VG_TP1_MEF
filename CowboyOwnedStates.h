#ifndef COWBOY_OWNED_STATES_H
#define COWBOY_OWNED_STATES_H
//------------------------------------------------------------------------
//
//  Name:   CowboyOwnedStates.h
//
//  Desc:   All the states that can be assigned to the Cowboy class.
//
//
//------------------------------------------------------------------------
#include "fsm/State.h"


class Cowboy;
struct Telegram;




//------------------------------------------------------------------------
//
//  In this state the Cowboy will go outside of town (work, sleep, etc.).
//  Then after enough time outside, he goes in the saloon, cheers
//  the miner if he's there then drink. After some drinks, he gets drunk,
//  threat the miner if he's in the saloon and return outside of town.
//
//  TODO : - multithread ?
//------------------------------------------------------------------------




// ---------
class CowboyGlobalState : public State<Cowboy>
{
private:

	CowboyGlobalState() {}

	//copy ctor and assignment should be private
	CowboyGlobalState(const CowboyGlobalState&);
	CowboyGlobalState& operator=(const CowboyGlobalState&);

public:

	//this is a singleton
	static CowboyGlobalState* Instance();

	virtual void Enter(Cowboy* agent) {}

	virtual void Execute(Cowboy* agent) {};

	virtual void Exit(Cowboy* agent) {}

	virtual bool OnMessage(Cowboy* agent, const Telegram& msg);
};


// ---------
class GoOutOfTown : public State<Cowboy>
{
private:

	GoOutOfTown() {}

	//copy ctor and assignment should be private
	GoOutOfTown(const GoOutOfTown&);
	GoOutOfTown& operator=(const GoOutOfTown&);

public:

	//this is a singleton
	static GoOutOfTown* Instance();

	virtual void Enter(Cowboy* cowboy);

	virtual void Execute(Cowboy* cowboy);

	virtual void Exit(Cowboy* cowboy);

	virtual bool OnMessage(Cowboy* agent, const Telegram& msg);

};

//------------------------------------------------------------------------
//
//  The Cowboy return at the saloon to get drunk
//------------------------------------------------------------------------
class GoInSaloonAndDrink : public State<Cowboy>
{
private:

	GoInSaloonAndDrink() {}

	//copy ctor and assignment should be private
	GoInSaloonAndDrink(const GoInSaloonAndDrink&);
	GoInSaloonAndDrink& operator=(const GoInSaloonAndDrink&);

public:

	//this is a singleton
	static GoInSaloonAndDrink* Instance();

	virtual void Enter(Cowboy* cowboy);

	virtual void Execute(Cowboy* cowboy);

	virtual void Exit(Cowboy* cowboy);

	virtual bool OnMessage(Cowboy* agent, const Telegram& msg);
};

//------------------------------------------------------------------------
//
//  The cowboy enter this phase if he's drunk and has someone to argue with
//  After this phase, he goes back outside pf town
//------------------------------------------------------------------------
class WantToFight : public State<Cowboy>
{
private:

	WantToFight() {}

	//copy ctor and assignment should be private
	WantToFight(const WantToFight&);
	WantToFight& operator=(const WantToFight&);

public:

	//this is a singleton
	static WantToFight* Instance();

	virtual void Enter(Cowboy* cowboy);

	virtual void Execute(Cowboy* cowboy);

	virtual void Exit(Cowboy* cowboy);

	virtual bool OnMessage(Cowboy* agent, const Telegram& msg);
};




#endif